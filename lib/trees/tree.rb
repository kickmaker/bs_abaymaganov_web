class Tree
  class Node
    attr_reader :data, :next_nodes, :parent

    def initialize(data)
      @data = data
      @parent = nil
      @next_nodes = []
    end

    def add_node(node)
      fail 'Incorrect method params' unless node.is_a? Tree::Node

      unless @next_nodes.include? node
        node.instance_exec(self) { |obj| @parent = obj }
        @next_nodes << node

        return node
      end

      nil
    end

    def delete_node(node)
      @next_nodes.delete(node)
    end

    def height
      node_height = 0

      unless @next_nodes.empty?
        max_node_height = 0

        @next_nodes.each do |node|
          next_node_height = node.height

          if max_node_height < next_node_height
            max_node_height = next_node_height
          end
        end

        node_height = max_node_height + 1
      end

      node_height
    end

    def depth
      node_depth = 0
      node_depth = parent.depth + 1 if parent

      node_depth
    end

    def ==(other)
      return super(other) unless other.is_a? Tree::Node
      @data == other.data
    end

    def eql?(other)
      (object_id == other.object_id) || (self == other)
    end
  end

  attr_reader :root

  def initialize(root)
    fail 'Incorrect initialize parameters' unless root.is_a? Tree::Node
    @root = root
  end

  def height
    @root.height
  end
end
