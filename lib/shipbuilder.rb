class ShipBuilder
  def initialize(deck_size, possible_choices, input, placing_strategy)
    @deck_size = deck_size
    @possible_choices = possible_choices
    @input = input
    @placing_strategy = placing_strategy
  end

  def try_to_place!
    return false if @deck_size < 1
    return false if @possible_choices.empty?

    possible_choices = @possible_choices
    begin
      # choose first cell within possible_choices
      coord = get_coord(@input, possible_choices)
      return false unless coord

      # build input variations tree for chosen coordinate
      tree_builder_options = {
        root: coord,
        possible_choices: @possible_choices,
        placing_strategy: @placing_strategy,
        decks: @deck_size
      }
      tree_builder = InputVariationsTreeBuilder.new(tree_builder_options)
      variations_tree = tree_builder.build

      # when too little space for the ship with deck_size in chosen coordinate
      can_built_ship = (variations_tree.height + 1) >= @deck_size
      unless can_built_ship
        possible_choices.delete(coord)
        return false if possible_choices.empty?
      end
    end until can_built_ship

    # choose other coordinates for ships and push it to decks
    cur_node = variations_tree.root
    decks = [coord]
    @deck_size.pred.times do
      next_nodes = cur_node.next_nodes

      next_choices = next_nodes.select do |node|
        is_good_branch = node.height == variations_tree.height - node.depth
        is_good_choose = possible_choices.include? node.coordinates

        is_good_branch && is_good_choose
      end.map(&:coordinates)

      next_coord = get_coord(@input, next_choices)
      return false unless next_coord

      decks << next_coord
      next_nodes.each { |node| cur_node = node if node.coordinates == next_coord }
    end

    Ship.new(decks)
  end

  private

  def get_coord(input, possible_choices)
    return nil if possible_choices.empty?

    case input.class.name
    when 'RandomInput'
      return RandomInput.new(possible_choices).fetch

    when 'QueueInput'
      loop do
        coord = input.fetch

        return coord if possible_choices.include? coord
        return nil unless coord
      end

    else
      fail 'Undefined input type'
    end
  end
end
