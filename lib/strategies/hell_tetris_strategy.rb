class HellTetrisStrategy < DefaultStrategy
  def self.max_dimension
    4
  end

  def places
    return super unless @decks == self.class.max_dimension

    return [] unless @positions.count < @decks

    if @positions.empty?
      possible_places = Array.new(Board::COLS) do |x|
        Array.new(Board::ROWS) do |y|
          Coordinates.new(x, y)
        end
      end.flatten(1)

      return possible_places
    end

    # gap detection
    @positions.each { |deck| return [] if neighbors(deck).count == 0 } if @positions.count > 1

    is_vert_ship = is_horz_ship = true
    (1..@positions.size - 1).each do |i|
      is_vert_ship &&= (@positions[i - 1].x == @positions[i].x)
      is_horz_ship &&= (@positions[i - 1].y == @positions[i].y)
    end

    if is_vert_ship
      possible_places = []
      possible_places << Coordinates.new(@positions.first.x, @positions.first.y - 1)
      possible_places << Coordinates.new(@positions.last.x, @positions.last.y + 1)

      @positions.each do |deck|
        possible_places << Coordinates.new(deck.x - 1, deck.y)
        possible_places << Coordinates.new(deck.x + 1, deck.y)
      end
    elsif is_horz_ship
      possible_places = []
      possible_places << Coordinates.new(@positions.first.x - 1, @positions.first.y)
      possible_places << Coordinates.new(@positions.last.x + 1, @positions.last.y)

      @positions.each do |deck|
        possible_places << Coordinates.new(deck.x, deck.y - 1)
        possible_places << Coordinates.new(deck.x, deck.y + 1)
      end
    else # is corner
      possible_places = []

      @positions.each do |deck|
        neighbors(deck).each do |neighbor|
          top_neighbor = (deck.y - neighbor.y) > 0
          bottom_neighbor = (deck.y - neighbor.y) < 0
          left_neighbor = (deck.x - neighbor.x) > 0
          right_neighbor = (deck.x - neighbor.x) < 0

          possible_places << Coordinates.new(deck.x, deck.y + 1) if top_neighbor
          possible_places << Coordinates.new(deck.x, deck.y - 1) if bottom_neighbor
          possible_places << Coordinates.new(deck.x + 1, deck.y) if left_neighbor
          possible_places << Coordinates.new(deck.x - 1, deck.y) if right_neighbor
        end
      end
    end

    Board.clean_outer(possible_places)
  end
end
