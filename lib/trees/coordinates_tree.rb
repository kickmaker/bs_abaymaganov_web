class CoordinatesTree < Tree
  class Node < Tree::Node
    def initialize(coord)
      fail 'Incorrect initialize parameters' unless coord.is_a? Coordinates
      super(coord)
    end

    def add_node(node)
      fail 'Incorrect method params' unless node.is_a? CoordinatesTree::Node
      super(node)
    end

    def coordinates
      data
    end

    protected

    attr_reader :data
  end

  def initialize(root)
    fail 'Incorrect initialize parameters' unless root.is_a? CoordinatesTree::Node
    super(root)
  end
end
