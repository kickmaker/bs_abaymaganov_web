class InputVariationsTreeBuilder
  def initialize(options)
    @root = options[:root]
    @possible_choices = options[:possible_choices]
    @placing_strategy = options[:placing_strategy]
    @decks = options[:decks]
  end

  # return tree of the variations of the player choices for coord
  def build
    variations_tree = InputVariationsTree.new(InputVariationsTree::Node.new(@root))
    variations_nodes_for([@root]).each { |node| variations_tree.root.add_node(node) }
    variations_tree
  end

  private

  # return nodes for next levels of the tree
  def variations_nodes_for(positions)
    # create strategy class
    strategy = @placing_strategy.new(positions, @decks)

    # recursive nodes creation for next levels
    nodes = []
    strategy.places.each do |place|
      next unless @possible_choices.include? place
      node = InputVariationsTree::Node.new(place)
      new_positions = positions + [place]
      variations_nodes_for(new_positions).each { |next_node| node.add_node(next_node) }

      nodes << node
    end

    nodes
  end
end
