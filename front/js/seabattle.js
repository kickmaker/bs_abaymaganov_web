/**
 * Created by dogmat on 10.03.16.
 */
// well, it's like constants
var ROWS = 10;
var COLS = 10;
var STAGE = {
    game_prepare: 0,
    building_start: 1,
    building_on: 2,
    game_begun: 3,
    player_win: 4,
    player_lose: 5,
    game_cant_start: 6
};

jQuery(function($) {

// jQuery extensions section
    $.fn.removeClassWild = function(mask) {
        return this.removeClass(function(index, cls) {
            var re = mask.replace(/\*/g, '\\S+');
            return (cls.match(new RegExp('\\b' + re + '', 'g')) || []).join(' ');
        });
    };

    $.selectorExists = function(selector) {
        return $(selector).length > 0;
    };

// auxiliary objects constructors section
    // constructor to initialize Board object
    function Board(selector, name) {
        this.dom = function() {
            return this._dom;
        };

        this.name = function() {
            return this._dom.data('name');
        };

        this.filled_space = function() {
            return $(this._filled_space);
        };

        this.init_board = function() {
            for (var j = 0; j < ROWS; ++j) {
                var row = $('<div></div>').appendTo(this._dom);

                for (var i = 0; i < COLS; ++i) {
                    var cell = $('<div class="empty_cell">&nbsp</div>').appendTo(row);
                    cell.data('x', i).data('y', j);
                }
            }
        };

        this.cells = function(cells_class) {
            var cells_selector = cells_class;

            if (typeof(cells_selector) == 'undefined' || cells_selector.length == 0)
                cells_selector = 'div div';

            return $(this._selector + ' ' + cells_selector);
        };

        this.cell = function(x, y) {
            var cell = null;

            $(this._selector + ' div div').each(function(i, elem) {
                var cl = $(elem);

                if ( (cl.data('x') == x) && (cl.data('y') == y) ) {
                    cell = cl
                }
            });

            return cell
        };

        this.update_filled_space = function(space) {
            this._filled_space = this._filled_space.concat(space);
        };

        this.clear_filled_space = function() {
            this._filled_space = [];
        };

    // private properties
        this._selector = selector;

        this._dom = $(selector);
        this._dom.data('type', 'board');
        this._dom.data('name', name);

        this._filled_space = [];
    }

    // constructor to initialize building_ship object
    function BuildingShip(name, size) {
    // use it instead properties
        this.name = function() {
            return this._name;
        };

        this.size = function() {
            return this._size;
        };

        this.decks = function() {
            return this._decks;
        };

        this.add_deck = function(cell_elem) {
            var decks_board = cell_elem.parent().parent();

            if (decks_board.data('type') != 'board') {
                console.error('building ship: try add deck with NO BOARD parent');

                return;
            }

            if (typeof(this._board) == 'undefined')
                this._board = decks_board;

            if (this._board.data('name') != decks_board.data('name')) {
                console.error('building ship: try add deck with different parent board');

                return;
            }

            if (this._decks.length < this._size)
                this._decks.push(cell_elem);
            else
                console.error('building ship: decks overflow');
        };

        this.built = function() {
            return (this._size == this._decks.length);
        };

        this.placed = function() {
            return this._placed;
        };

        this.confirm_placing = function() {
            $.ajax({
                url: 'place',
                method: 'POST',
                data: this._json(),
                async: false,
                statusCode: {
                    200: this._response_ok,
                    422: this._unprocessable_entity,
                    500: this._internal_server_error
                }
            })
        };

        this.filled_space = function() {
            var coordinates = [];

            this._decks.forEach(function(deck) {
                for (var i = -1; i <= 1; ++i) {
                    for (var j = -1; j <= 1; ++j) {
                        var coord = new Coordinates(deck.data('x') + i, deck.data('y') + j);
                        var contains_already = coordinates.some(function(c) {
                            return ( c.x() == coord.x() && c.y() == coord.y() );
                        });

                        if (!contains_already)
                            coordinates.push(coord);
                    }
                }
            });

            var space = [];

            this._board.children().each(function(i, row) {
                $(row).children().each(function(i, cell) {
                    var is_include_to_space = coordinates.some(function(c) {
                        return ( c.x() == $(cell).data('x') && c.y() == $(cell).data('y'));
                    });

                    if (is_include_to_space)
                        space.push(cell);
                })
            });

            return space;
        };

    // private
        this._json = function() {
            var data = [];

            this._decks.forEach(function(deck) {
                data.push({
                    x: deck.data('x'),
                    y: deck.data('y')
                });
            });

            return JSON.stringify(data)
        };

        this._response_ok = function() {
            // tell me how do it else!
            game.building_ship()._placed = true
        };

        this._unprocessable_entity = function() {
            game.building_ship()._placed = false;
            alert('422: Unprocessable entity.\n' +
                  'Ships placing fail.')
        };

        this._internal_server_error = function() {
            game.building_ship()._placed = false;
            alert('500: Internal server error.\n' +
                  'Ships placing fail.')
        };

        this._name = name;

        this._size = size;

        this._placed = false;

        this._board = undefined;

        // contains DOM elements (.ship_cell)
        this._decks = [];
    }

    // game object contains info for callback functions using
    function Game() {
    // use it instead properties
        this.stage = function(){
            return this._stage;
        };

        this.set_stage_by_name = function(stage){
            this._stage = STAGE[stage];
        };

        this.building_ship = function() {
            return this._building_ship;
        };

        this.set_building_ship = function(ship) {
            this._building_ship = ship;
        };

        this.ships_left = function(ship_name) {
            if (typeof(ship_name) == 'undefined' || ship_name.length == 0) {
                var ships_left = 0;

                for (var key in this._ships_to_place)
                    if (this._ships_to_place.hasOwnProperty(key))
                        ships_left += this._ships_to_place[key];

                return ships_left;
            }

            return this._ships_to_place[ship_name];
        };

        this.all_ships_placed = function() {
            return this.ships_left() == 0;
        };

        this.reduce_ships_count = function(ship_name) {
            return this._ships_to_place[ship_name] -= 1;
        };

        this.confirm_start = function() {
            $.ajax({
                url: 'start',
                method: 'POST',
                async: false,
                statusCode: {
                    200: this._response_ok,
                    417: this._expectation_failed,
                    500: this._internal_server_error
                }
            })
        };

    // private
        this._response_ok = function() {
            game._stage = STAGE['game_begun']
        };

        this._expectation_failed = function() {
            game._stage = STAGE['game_cant_start'];
            alert('417: Expectation Failed.\n' +
                    'Can\'t start the game.')
        };

        this._internal_server_error = function() {
            game._stage = STAGE['game_cant_start'];
            alert('500: Internal Server Error.\n' +
                    'Can\'t start the game.')
        };

        this._ships_to_place = {
            one_decker: 4,
            two_decker: 3,
            three_decker: 2,
            four_decker: 1
        };

        this._building_ship = undefined;
        this._stage = STAGE['game_prepare'];
    }

// callbacks section
    function init_callbacks() {
        $('.one_decker_for_placing').bind('click', ship_for_placing_click('one_decker', 1));
        $('.two_decker_for_placing').bind('click', ship_for_placing_click('two_decker', 2));
        $('.three_decker_for_placing').bind('click', ship_for_placing_click('three_decker', 3));
        $('.four_decker_for_placing').bind('click', ship_for_placing_click('four_decker', 4));
        $('.you_won_btn').bind('click', restart_game);
        $('.restart_btn').bind('click', restart_game);
        $('.you_lose_btn').bind('click', restart_game);
        $(document).bind('keyup', document_key_up);
    }

    function document_key_up(event) {
        // esc
        if (event.keyCode == 27) {
            cancel_ship_building()
        }
    }

    function ship_for_placing_click(ship_name, ship_size) {
        return function(event) {
            if (game.stage() == STAGE['game_prepare'] && game.ships_left(ship_name) > 0) {
                make_red_deck($(this).children('.blue_deck'));
                game.set_building_ship(new BuildingShip(ship_name, ship_size));
                game.set_stage_by_name('building_start');
                update_boards_state(game.stage());
            }
        }
    }

    function empty_cell_click(ship_name) {
        return function(event) {
            var building_ship = game.building_ship();

            var this_cell = make_ship_cell($(this));
            this_cell.unbind('click');
            building_ship.add_deck(this_cell);

            var red_decks = $('.' + ship_name + '_for_placing .red_deck');
            var last = red_decks.size() - 1;
            make_blue_deck(red_decks.eq(last));

            if (building_ship.built()) {
                building_ship.confirm_placing();

                if (building_ship.placed()) {
                    player_board.update_filled_space(building_ship.filled_space());
                    reduce_ships_counter(ship_name);

                    if (game.ships_left(ship_name) == 0)
                        freeze_ship_selector(ship_name);

                    if (game.all_ships_placed()) {
                        game.confirm_start();
                    }
                    else
                        game.set_stage_by_name('game_prepare');
                }
                else {
                    cancel_ship_building();
                    alert('Ship wasn\'t built. Please repeat placing.')
                }
            }
            else {
                game.set_stage_by_name('building_on');
            }

            update_boards_state(game.stage());
        }
    }

    function filled_cell_click(event) {
        blink_cell($(this), '#FF0000', 300)
    }

    function enemy_cell_click(event) {
        var enemy_cell = $(this);
        var json_obj = {x: enemy_cell.data('x'), y: enemy_cell.data('y')};

        $.ajax({
            url: 'shot',
            method: 'POST',
            async: true,
            dataType: 'json',
            data: JSON.stringify(json_obj),
            success: function(data, textStatus, jqXHR) {
                if (jqXHR.status != 200) {
                    alert (textStatus);
                    return;
                }

                if (data.hit) {
                    make_hit_cell(enemy_cell);
                    blink_cell(enemy_cell, '#FF0000', 300);
                    enemy_cell.unbind('click');
                }
                else {
                    make_miss_cell(enemy_cell);
                    blink_cell(enemy_cell, '#0072FF', 300);
                    enemy_cell.unbind('click');
                }

                var enemy_shot = data.enemy_shot;
                if (enemy_shot) {
                    var player_cell = player_board.cell(enemy_shot.x, enemy_shot.y);

                    if (enemy_shot.hit) {
                        make_hit_cell(player_cell);
                        blink_cell(player_cell, '#FF0000', 300);
                    }
                    else {
                        make_miss_cell(player_cell);
                        blink_cell(player_cell, '#0072FF', 300);
                    }
                }

                switch (data.state) {
                    case 0: {
                        break;
                    }
                    case 1: {
                        game.set_stage_by_name('player_win');
                        update_boards_state(game.stage());

                        break;
                    }
                    case 2: {
                        game.set_stage_by_name('player_lose');
                        update_boards_state(game.stage());

                        break;
                    }
                    default: {
                    }
                }
            }
        })
    }

    function update_boards_state(stage) {
        switch (stage) {
            case STAGE['game_prepare']: {
                var player_board_cells = player_board.cells();
                player_board_cells.unbind('click');
                make_empty_cell(player_board_cells.not('.ship_cell'));

                break;
            }
            case STAGE['building_start']: {
                var filled_cells = player_board.filled_space();
                make_fill_cell(filled_cells.not('.ship_cell'));
                filled_cells.bind('click', filled_cell_click);
                player_board.cells('.empty_cell').bind('click', empty_cell_click(game.building_ship().name()));

                break;
            }
            case STAGE['building_on']: {
                // create placing strategy
                var building_ship = game.building_ship();
                var decks = building_ship.decks().map(function(elem) {
                    return new Coordinates(elem.data('x'), elem.data('y'));
                });
                var placing_strategy = new PlacingStrategy(decks, building_ship.size());

                // get cells available to choose
                var possible_choices = placing_strategy.places();
                var filled_space = player_board.filled_space();
                var available_to_choose_cells = player_board.cells().filter(function(i, cell) {
                    var is_filled_cell_already =  filled_space.get().some(function(place) {
                        return ( $(place).data('x') == $(cell).data('x') && $(place).data('y') == $(cell).data('y') );
                    });

                    var is_possible_to_choose = possible_choices.some(function(place) {
                        return ( place.x() == $(cell).data('x') && place.y() == $(cell).data('y') );
                    });

                    return !is_filled_cell_already && is_possible_to_choose;
                });

                // change cells style, bind/unbind event handlers
                var player_board_cells = player_board.cells();
                player_board_cells.unbind('click');

                var filled_cells = player_board_cells.not('.ship_cell').not(available_to_choose_cells);
                make_fill_cell(filled_cells);
                filled_cells.bind('click', filled_cell_click);
                player_board.cells('.ship_cell').bind('click', filled_cell_click);

                make_empty_cell(available_to_choose_cells);
                available_to_choose_cells.bind('click', empty_cell_click(building_ship.name()));

                break;
            }
            case STAGE['game_begun']: {
                var player_board_cells = player_board.cells();
                player_board_cells.unbind('click');
                make_empty_cell(player_board_cells.not('.ship_cell'));

                var enemy_board_cells = enemy_board.cells();
                enemy_board_cells.bind('click', enemy_cell_click);

                alert('Game Started!');

                break;
            }
            case STAGE['player_win']: {
                player_board.cells().unbind('click');
                enemy_board.cells().unbind('click');
                show_win();

                break;
            }
            case STAGE['player_lose']: {
                player_board.cells().unbind('click');
                enemy_board.cells().unbind('click');
                show_lose();

                break;
            }
            default: {
            }
        }
    }

    function restart_game() {
        $.ajax({
            url: 'restart',
            method: 'POST',
            success: function(data, textStatus, jqXHR) {
                if (jqXHR.status != 200) {
                    alert (textStatus);
                    return;
                }

                // renew game object
                game = new Game();

                // restore ships selectors and ships counters
                for (var ship_name in game._ships_to_place) {
                    var ship_selector = $('.' + ship_name + '_for_placing');
                    var ship_counter = $('.' + ship_name + '_counter');

                    ship_selector.unbind('click');
                    make_blue_deck(ship_selector.children().not(ship_counter));
                    ship_counter.text(game._ships_to_place[ship_name]);
                }

                // restore callbacks
                $('.restart_btn, .you_won_btn, .you_lose_btn').unbind('click');
                $(document).unbind('keyup');
                init_callbacks();

                // clear boards cells
                var player_board_cells = player_board.cells();
                player_board_cells.unbind('click');
                make_empty_cell(player_board_cells);
                player_board.clear_filled_space();

                var enemy_board_cells = enemy_board.cells();
                enemy_board_cells.unbind('click');
                make_empty_cell(enemy_board_cells);
                enemy_board.clear_filled_space();

                // show/hide control buttons
                show_restart();

                alert('Game restarted!')
            }
        })
    }

    function cancel_ship_building() {
        var building_ship = game.building_ship();

        var red_decks = $('.' + building_ship.name() + '_for_placing .red_deck');
        make_blue_deck(red_decks);

        game.building_ship().decks().forEach(function(deck) {
            make_empty_cell($(deck));
        });

        game.set_stage_by_name('game_prepare');
        update_boards_state(game.stage());
    }

    function blink_cell(cell, color, duration) {
        var prev_color = cell.css('backgroundColor');

        function after_animate() {
            cell.removeAttr('style')
        }

        cell.animate({backgroundColor: color}, duration, after_animate);
        cell.animate({backgroundColor: prev_color}, duration, after_animate);
    }

    function reduce_ships_counter(ship_name) {
        var new_count = game.reduce_ships_count(ship_name);
        $('.' + ship_name + '_counter').text(new_count);
    }

    function freeze_ship_selector(ship_name) {
        make_gray_deck($('.' + ship_name + '_for_placing .blue_deck'));
        $('.' + ship_name + '_for_placing').unbind('click');
    }

    function make_ship_cell(cell_elem) {
        return cell_elem.removeClassWild('*_cell').addClass('ship_cell').html('&nbsp');
    }

    function make_fill_cell(cell_elem) {
        return cell_elem.removeClassWild('*_cell').addClass('filled_cell').html('&nbsp');
    }

    function make_empty_cell(cell_elem) {
        return cell_elem.removeClassWild('*_cell').addClass('empty_cell').html('&nbsp');
    }

    function make_hit_cell(cell_elem) {
        return cell_elem.removeClassWild('*_cell').addClass('hit_cell').html('<sub><b>X</b></sub>');
    }

    function make_miss_cell(cell_elem) {
        return cell_elem.removeClassWild('*_cell').addClass('miss_cell').html('<small>&#9679</small>');
    }

    function make_red_deck(deck_elem) {
        return deck_elem.removeClassWild('*_deck').addClass('red_deck');
    }

    function make_blue_deck(deck_elem) {
        return deck_elem.removeClassWild('*_deck').addClass('blue_deck');
    }

    function make_gray_deck(deck_elem) {
        return deck_elem.removeClassWild('*_deck').addClass('gray_deck');
    }

    function show_win() {
        $('.you_lose_btn').hide();
        $('.restart_btn').hide();
        $('.you_won_btn').show();
    }

    function show_lose() {
        $('.you_won_btn').hide();
        $('.restart_btn').hide();
        $('.you_lose_btn').show();
    }

    function show_restart() {
        $('.you_won_btn').hide();
        $('.you_lose_btn').hide();
        $('.restart_btn').show();
    }

// main section
    var game = new Game();
    var player_board = new Board('.player_board', 'player');
    var enemy_board = new Board('.enemy_board', 'enemy');

    player_board.init_board();
    enemy_board.init_board();
    init_callbacks();
    show_restart();
});
