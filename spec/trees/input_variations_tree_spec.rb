require 'coordinates'
require 'trees/tree'
require 'trees/coordinates_tree'
require 'trees/input_variations_tree'

describe InputVariationsTree do
  before(:all) do
    @root = InputVariationsTree::Node.new Coordinates.new(0, 0)
    @tree = InputVariationsTree.new @root

    @child1_0 = InputVariationsTree::Node.new Coordinates.new(1, 0)
    @child1_1 = InputVariationsTree::Node.new Coordinates.new(1, 1)
    @child1_2 = InputVariationsTree::Node.new Coordinates.new(1, 2)
    @child2_0 = InputVariationsTree::Node.new Coordinates.new(2, 0)
    @child2_1 = InputVariationsTree::Node.new Coordinates.new(2, 1)
  end

  context 'when node has been initialized with coordinates' do
    it 'must have coordinates equal (0, 0)' do
      expect(@root.coordinates).to eq Coordinates.new(0, 0)
    end

    it 'must have no parent' do
      expect(@root.parent).to be_nil
    end

    it 'must have no next nodes' do
      expect(@root.next_nodes).to be_empty
    end

    it 'must have height equal 0' do
      expect(@root.height).to be == 0
    end

    it 'must have depth equal 0' do
      expect(@root.height).to be == 0
    end
  end

  context 'when node has been initialized with not coordinates type' do
    it 'must fail with message' do
      expect do
        InputVariationsTree::Node.new 'wrong data'
      end.to raise_error('Incorrect initialize parameters')
    end
  end

  context 'when tree initialized' do
    it 'tree must have root equal node from init params' do
      expect(@tree.root).to be == @root
    end
  end

  context 'when root node has added 3 child nodes to itself' do
    it 'root must have 3 next nodes' do
      @root.add_node @child1_0
      @root.add_node @child1_1
      @root.add_node @child1_2

      expect(@root.next_nodes).to include @child1_0
      expect(@root.next_nodes).to include @child1_1
      expect(@root.next_nodes).to include @child1_2
      expect(@root.next_nodes.size).to be == 3
    end

    it 'tree and root must have height equal 1' do
      expect(@tree.height).to be == 1
      expect(@root.height).to be == 1
    end

    it 'each of next nodes must have depth equal 1' do
      @root.next_nodes.each do |node|
        expect(node.depth).to be == 1
      end
    end

    it 'each of next nodes must have height equal 0' do
      @root.next_nodes.each do |node|
        expect(node.height).to be == 0
      end
    end

    it 'each of next nodes must have parent equal root node' do
      @root.next_nodes.each do |node|
        expect(node.parent).to be == @root
      end
    end
  end

  context 'when root node has added child node that is already contained in' do
    it 'must avoid node duplication' do
      cnt_before_add = @root.next_nodes.size
      @root.add_node @child1_0
      cnt_after_add = @root.next_nodes.size

      expect(cnt_before_add).to be == cnt_after_add
    end
  end

  context 'when first branch has two new added nodes' do
    it 'tree and root must have height equal 2' do
      @child1_0.add_node @child2_0
      @child1_0.add_node @child2_1
      expect(@root.height).to be == 2
    end

    it 'first branches leaf nodes must have depth equal 2' do
      expect(@child2_0.depth).to be == 2
      expect(@child2_1.depth).to be == 2
    end

    it 'second and third branches leaf nodes must have depth equal 1' do
      expect(@child1_1.depth).to be == 1
      expect(@child1_2.depth).to be == 1
    end
  end

  context 'when first branch has deleted' do
    it 'root must have 2 next nodes' do
      @root.delete_node @child1_0
      expect(@root.next_nodes.size).to be == 2
    end

    it 'tree and root must have height eq 1' do
      expect(@tree.height).to be == 1
      expect(@root.height).to be == 1
    end
  end
end
