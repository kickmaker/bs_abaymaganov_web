require 'inputs/some_input'
require 'inputs/random_input'

describe RandomInput do
  describe '#fetch' do
    before(:all) do
      @inputs = [[1, 1], [2, 2], [3, 3]]
      @input = RandomInput.new(@inputs)
    end

    context 'when fetch calls' do
      it 'should return any value from inputs' do
        value = @input.fetch
        expect(@inputs).to include value
      end
    end
  end
end
