class Game
  STATE = {
    uninitialized: 0,
    configured: 1,
    prepared: 2,
    started: 3,
    player_win: 4,
    enemy_win: 5
  }.freeze

  attr_reader :state

  def initialize
    @state = STATE[:uninitialized]
  end

  def configure(config)
    return unless @state == STATE[:uninitialized]

    @config = config
    @state = STATE[:configured]
  end

  def prepare
    return unless @state == STATE[:configured]

    check_file_path(@config[:player_board_file])
    check_file_path(@config[:enemy_board_file])
    check_file_path(@config[:enemy_shooting_file])

    @player_board = Board.new(@config[:board_options], @config[:strategy])
    @player_board.serialize(@config[:player_board_file])

    @enemy_board = Board.new(@config[:board_options], @config[:strategy])
    @enemy_board.fill_with_ships!
    @enemy_board.serialize(@config[:enemy_board_file])

    @player = RealPlayer.new(@enemy_board)
    @enemy = VirtualPlayer.new(@player_board)

    File.delete @config[:enemy_shooting_file] if File.exist? @config[:enemy_shooting_file]

    @state = STATE[:prepared]
  end

  def place_ship(ship)
    return nil unless @state == STATE[:prepared]

    @player_board.deserialize(@config[:player_board_file])

    return nil unless @player_board.place_ship(ship)

    @player_board.serialize(@config[:player_board_file])
    ship
  end

  def start
    return unless @state == STATE[:prepared]

    @player_board.deserialize(@config[:player_board_file])
    @enemy_board.deserialize(@config[:enemy_board_file])

    @state = STATE[:started] if @player_board.all_ships_placed? && @enemy_board.all_ships_placed?
  end

  def shoot(coord)
    return unless @state == STATE[:started]

    @player_board.deserialize(@config[:player_board_file])
    @enemy_board.deserialize(@config[:enemy_board_file])
    @enemy.deserialize(@config[:enemy_shooting_file]) if File.exist? @config[:enemy_shooting_file]

    player_shot = @player.shoot(coord)
    # when enemy board coordinate not exists
    return nil if player_shot.nil?

    enemy_shot = nil
    if @enemy_board.any_life?
      enemy_shot = @enemy.shoot
      return nil if enemy_shot.nil?

      @enemy.serialize(@config[:enemy_shooting_file])
    end

    @player_board.serialize(@config[:player_board_file])
    @enemy_board.serialize(@config[:enemy_board_file])

    @state = STATE[:player_win] unless @enemy_board.any_life?
    @state = STATE[:enemy_win] unless @player_board.any_life?

    [player_shot, enemy_shot]
  end

  private

  def check_file_path(file_name)
    dir = File.dirname(file_name)
    FileUtils.mkdir_p dir unless File.directory? dir
  end
end
