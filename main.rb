require_relative 'lib/lib_loader'
require 'bundler'

Bundler.require

class App < Sinatra::Application
  CONFIG = {
      board_options: { 4 => 1, 3 => 2, 2 => 3, 1 => 4 },
      strategy: HellTetrisStrategy,
      player_board_file: './csv/player_board.csv',
      enemy_board_file: './csv/enemy_board.csv',
      enemy_shooting_file: './csv/enemy_shooting.csv'
  }.freeze

  @@game = nil

  get '/' do
    @@game = Game.new
    @@game.configure(CONFIG)
    @@game.prepare

    send_file './front/index.html'
  end

  get '/:dir/:file' do
    send_file "./front/#{params[:dir]}/#{params[:file]}"
  end

  post '/restart' do
    begin
      @@game = Game.new
      @@game.configure(CONFIG)
      @@game.prepare
      status 200

    rescue Exception => ex
      puts ex.message
      status 500
    end
  end

  post '/place' do
    return 422 unless request.xhr?

    begin
      json_data = JSON.parse request.body.read
      coords = json_data.map { |elem| Coordinates.new(elem) }
      ship = Ship.new(coords)
      res = @@game.place_ship(ship)

      status 200
      status 422 unless res

    rescue ShipPlacingException => ex
      puts ex.message
      status 422

    rescue Exception => ex
      puts ex.message
      status 500
    end
  end

  post '/start' do
    begin
      @@game.start
      status 200
      status 417 unless @@game.state == Game::STATE[:started]

    rescue Exception => ex
      puts ex.message
      status 500
    end
  end

  post '/shot' do
    return 422 unless request.xhr?

    begin
      json_reqv_data = JSON.parse request.body.read
      shoot_to = Coordinates.new(json_reqv_data)
      res = @@game.shoot(shoot_to)

      if res
        player_shot = res[0]
        enemy_shot = res[1]
        game_state = case @@game.state
                     when Game::STATE[:started] then 0
                     when Game::STATE[:player_win] then 1
                     when Game::STATE[:enemy_win] then 2
                     else #todo what else?
                     end

        json_resp_data = {
          state: game_state,
          hit: player_shot.result,
          enemy_shot: enemy_shot ? {
            hit: enemy_shot.result,
            x: enemy_shot.coordinates.x,
            y: enemy_shot.coordinates.y
          } : nil
        }.to_json

        status 200
        content_type :json
        body json_resp_data
      else
        status 422
      end

    rescue Exception => ex
      puts ex.message
      status 500
    end
  end

  run! if app_file == $0
end
