require 'coordinates'
require 'cells/cell'
require 'cells/deck'

describe Deck do
  context 'when check_it!' do
    it 'have state "hit"' do
      deck = Deck.new Coordinates.new(0, 0)
      deck.check_it!
      expect(deck.state).to eq :hit
    end
  end
end
