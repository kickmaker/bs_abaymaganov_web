/**
 * Created by dogmat on 14.03.16.
 */

function PlacingStrategy(positions, decks_count) {
// public
    this.places = function() {
        if (this._positions >= this._decks_count)
            return [];

        if (this._positions.length == 0) {
            var places = [];

            for (var i = 0; i < COLS; ++i)
                for (var j = 0; j < ROWS; ++j)
                    places.push(new Coordinates(i, j));

            return places;
        }

        // gap detection
        if (this._positions.length > 1) {
            for (var i = 0; i < this._positions.length; ++i) {
                var deck = this._positions[i];

                if (this._neighbors(deck).length == 0)
                    return [];
            }
        }

        var is_vert_ship = true;
        var is_horz_ship = true;
        for (var i = 1; i < this._positions.length; ++i) {
            is_vert_ship = is_vert_ship && (this._positions[i - 1].x() == this._positions[i].x());
            is_horz_ship = is_horz_ship && (this._positions[i - 1].y() == this._positions[i].y());
        }

        var places = [];
        if (is_vert_ship) {
            var last = this._positions.length - 1;
            places.push(new Coordinates(this._positions[0].x(), this._positions[0].y() - 1));
            places.push(new Coordinates(this._positions[last].x(), this._positions[last].y() + 1));

            if ((this._positions.length == 1) || (this._decks_count > 3)) {
                this._positions.forEach(function (deck) {
                    places.push(new Coordinates(deck.x() - 1, deck.y()));
                    places.push(new Coordinates(deck.x() + 1, deck.y()));
                })
            }
        }
        else
        if (is_horz_ship) {
            var last = this._positions.length - 1;
            places.push(new Coordinates(this._positions[0].x() - 1, this._positions[0].y()));
            places.push(new Coordinates(this._positions[last].x() + 1, this._positions[last].y()));

            if ((this._positions.length == 1) || (this._decks_count > 3)) {
                this._positions.forEach(function (deck) {
                    places.push(new Coordinates(deck.x(), deck.y() - 1));
                    places.push(new Coordinates(deck.x(), deck.y() + 1));
                })
            }
        }
        else {
            var strategy = this;

            strategy._positions.forEach(function(deck) {
                strategy._neighbors(deck).forEach(function(neigh) {
                    var top_neighbor = (deck.y() - neigh.y()) > 0;
                    var bottom_neighbor = (deck.y() - neigh.y()) < 0;
                    var left_neighbor = (deck.x() - neigh.x()) > 0;
                    var right_neighbor = (deck.x() - neigh.x()) < 0;

                    if (top_neighbor)
                        places.push(new Coordinates(deck.x(), deck.y() + 1));
                    if (bottom_neighbor)
                        places.push(new Coordinates(deck.x(), deck.y() - 1));
                    if (left_neighbor)
                        places.push(new Coordinates(deck.x() + 1, deck.y()));
                    if (right_neighbor)
                        places.push(new Coordinates(deck.x() - 1, deck.y()));
                })
            })
        }

        return this._clean_outer(places);
    };

// private
// init and sort positions
    this._positions = positions.sort(function(c1, c2) {
        if (c1.y() < c2.y())
            return -1;
        if (c1.y() > c2.y())
            return 1;
        return 0;
    }).sort(function(c1, c2) {
        if (c1.x() < c2.x())
            return -1;
        if (c1.x() > c2.x())
            return 1;
        return 0;
    });

    this._decks_count = decks_count;

    this._clean_outer = function(coords) {
        var inner_coords = [];

        coords.forEach(function(coord){
            var is_x_inside = (coord.x() >= 0) && (coord.x() <= 9);
            var is_y_inside = (coord.y() >= 0) && (coord.y() <= 9);

            if (is_x_inside && is_y_inside)
                inner_coords.push(coord);
        });

        return inner_coords;
    };

    this._positions_include = function (coord) {
        return this._positions.some(function(item) {
            return (item.x() == coord.x()) && (item.y() == coord.y());
        });
    };

    this._neighbors = function(deck) {
        var top = new Coordinates(deck.x(), deck.y() - 1);
        var bottom = new Coordinates(deck.x(), deck.y() + 1);
        var left = new Coordinates(deck.x() - 1, deck.y());
        var right = new Coordinates(deck.x() + 1, deck.y());

        var neighbors = [];

        if (this._positions_include(top))
            neighbors.push(top);
        if (this._positions_include(bottom)){
            neighbors.push(bottom);}
        if (this._positions_include(left))
            neighbors.push(left);
        if (this._positions_include(right))
            neighbors.push(right);

        return neighbors;
    }
}
