class Shot
  attr_reader :coordinates, :result

  def initialize(res, coord)
    @result = res
    @coordinates = coord
  end
end
