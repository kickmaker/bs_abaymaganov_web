class Shooter
  def initialize(board)
    @board = board
  end

  def shoot(coord)
    index = @board.field.index { |cell| cell.position == coord }
    return nil unless index

    state_before = @board.field[index].state
    state_after = @board.field[index].check_it!
    is_state_changed = (state_before != state_after)

    is_state_changed && (@board.field[index].state == :hit)
  end
end
