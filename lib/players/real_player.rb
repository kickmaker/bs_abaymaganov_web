class RealPlayer < SomePlayer
  def initialize(board)
    super(board)
  end

  def shoot(coord)
    was_hit = @shooter.shoot(coord)
    return nil if was_hit.nil?

    Shot.new(was_hit, coord)
  end
end
