class DefaultStrategy < BaseStrategy
  def places
    return [] unless @positions.count < @decks

    if @positions.empty?
      possible_places = Array.new(Board::COLS) do |x|
        Array.new(Board::ROWS) do |y|
          Coordinates.new(x, y)
        end
      end.flatten(1)

      return possible_places
    end

    # gap detection
    @positions.each { |deck| return [] if neighbors(deck).count == 0 } if @positions.count > 1

    is_vert_ship = is_horz_ship = true
    (1..@positions.size - 1).each do |i|
      is_vert_ship &&= (@positions[i - 1].x == @positions[i].x)
      is_horz_ship &&= (@positions[i - 1].y == @positions[i].y)
    end

    possible_places = []

    if is_vert_ship
      possible_places << Coordinates.new(@positions.first.x, @positions.first.y - 1)
      possible_places << Coordinates.new(@positions.last.x, @positions.last.y + 1)
    end

    if is_horz_ship
      possible_places << Coordinates.new(@positions.first.x - 1, @positions.first.y)
      possible_places << Coordinates.new(@positions.last.x + 1, @positions.last.y)
    end

    Board.clean_outer(possible_places)
  end

  protected

  def neighbors(deck)
    top = Coordinates.new(deck.x, deck.y - 1)
    bottom = Coordinates.new(deck.x, deck.y + 1)
    left = Coordinates.new(deck.x - 1, deck.y)
    right = Coordinates.new(deck.x + 1, deck.y)

    neighbors_arr = []
    neighbors_arr << top if @positions.include? top
    neighbors_arr << bottom if @positions.include? bottom
    neighbors_arr << left if @positions.include? left
    neighbors_arr << right if @positions.include? right

    neighbors_arr
  end
end
