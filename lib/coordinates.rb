class Coordinates
  attr_accessor :x, :y

  def initialize(*args)
    case args.length
    when 1
      arg = args.first

      if arg.is_a? Array
        arr = arg
        fail ArgumentError, 'Wrong argument array length' unless arr.size == 2

        @x = arr[0]
        @y = arr[1]
      end

      if arg.is_a? Hash
        hash = {}
        arg.each { |key, value| hash[key.to_sym] = value }

        unless hash.size == 2 && hash.key?(:x) && hash.key?(:y)
          fail ArgumentError, 'Wrong argument hash keys'
        end

        @x = hash[:x]
        @y = hash[:y]
      end

    when 2
      @x, @y = *args

    else
      fail ArgumentError, 'Too many arguments'
    end
  end

  def ==(coord)
    return super(coord) unless coord.is_a? Coordinates
    (@x == coord.x) && (@y == coord.y)
  end

  def hash
    (@x.to_s + @y.to_s).to_i
  end

  def eql?(other)
    (object_id == other.object_id) || (self == other)
  end
end
