class Ship
  attr_reader :coords
  attr_reader :decks

  def self.cell_surrounding(coord)
    surrounding = []

    (-1..1).each do |i|
      (-1..1).each do |j|
        surrounding << Coordinates.new(coord.x + i, coord.y + j)
      end
    end

    surrounding.delete_if do |coordinate|
      (coordinate.x < 0) || (coordinate.x > 9) ||
        (coordinate.y < 0) || (coordinate.y > 9) || (coordinate == coord)
    end
  end

  def initialize(items)
    if items.first.is_a? Deck
      @decks = Array.new(items)
      @coords = @decks.map(&:position)
    end

    if items.first.is_a? Coordinates
      @coords = Array.new(items)
      @decks = @coords.map { |coord| Deck.new(coord) }
    end
  end

  def size
    @decks.size
  end

  def killed?
    @decks.each { |deck| return false if deck.state == :unchecked }
  end

  def hit?
    @decks.any? { |deck| deck.state == :hit }
  end

  def filled_space
    space = []
    @coords.each { |coord| space |= (Ship.cell_surrounding(coord) << coord) }
    space
  end
end
