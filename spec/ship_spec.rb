require 'coordinates'
require 'cells/cell'
require 'cells/deck'
require 'ship'

describe Ship do
  context 'when calls filled_space' do
    it 'should return ship\'s surrounding coordinates + ship\'s decks coordinates' do
      decks_coordinates = [Coordinates.new(1, 1), Coordinates.new(2, 1)]

      expected_space = []
      (-1..2).each do |i|
        (-1..1).each do |j|
          expected_space << Coordinates.new(i + 1, j + 1)
        end
      end

      ship = Ship.new(decks_coordinates)
      filled_space = ship.filled_space.sort { |c1, c2| c1.y <=> c2.y }.sort { |c1, c2| c1.x <=> c2.x }

      expect(expected_space).to be == filled_space
    end
  end

  context 'when calls cell_surrounding' do
    it 'should return 8 coordinates of cell\'s surrounding for center' do
      cell = Coordinates.new(3, 3)

      expected_space = []
      (-1..1).each do |i|
        (-1..1).each do |j|
          expected_space << Coordinates.new(i + cell.x, j + cell.y)
        end
      end
      expected_space.delete(cell)

      surrounding = Ship.cell_surrounding(cell).sort { |c1, c2| c1.y <=> c2.y }.sort { |c1, c2| c1.x <=> c2.x }

      expect(surrounding).to be == expected_space
    end

    it 'should return 5 coordinates of cell\'s surrounding for border' do
      cell = Coordinates.new(0, 3)

      expected_space = []
      (0..1).each do |i|
        (-1..1).each do |j|
          expected_space << Coordinates.new(i + cell.x, j + cell.y)
        end
      end
      expected_space.delete(cell)

      surrounding = Ship.cell_surrounding(cell).sort { |c1, c2| c1.y <=> c2.y }.sort { |c1, c2| c1.x <=> c2.x }

      expect(surrounding).to be == expected_space
    end

    it 'should return 3 coordinates of cell\'s surrounding for corner' do
      cell = Coordinates.new(0, 0)

      expected_space = []
      (0..1).each do |i|
        (0..1).each do |j|
          expected_space << Coordinates.new(i + cell.x, j + cell.y)
        end
      end
      expected_space.delete(cell)

      surrounding = Ship.cell_surrounding(cell).sort { |c1, c2| c1.y <=> c2.y }.sort { |c1, c2| c1.x <=> c2.x }

      expect(surrounding).to be == expected_space
    end
  end
end
