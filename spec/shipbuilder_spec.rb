require 'lib_loader'

describe ShipBuilder do
  let(:possible_choices) do
    Array.new(Board::COLS) do |x|
      Array.new(Board::ROWS) do |y|
        Coordinates.new(x, y)
      end
    end.flatten(1)
  end

  let(:strategy) { HellTetrisStrategy }

  let(:coord1) { Coordinates.new(1, 1) }
  let(:coord2) { Coordinates.new(1, 2) }
  let(:coord3) { Coordinates.new(1, 3) }
  let(:coord4) { Coordinates.new(1, 4) }

  let(:inputs) { [coord1, coord2, coord3, coord4] }
  let(:queue_input) { QueueInput.new(inputs) }
  let(:random_input) { RandomInput.new(inputs) }
  let(:input) { random_input }

  describe '#try_to_place!' do
    context 'when deck size less than 1' do
      let(:decks) { 0 }
      let(:builder) { ShipBuilder.new(decks, possible_choices, input, strategy) }

      it 'should return false' do
        expect(builder.try_to_place!).to be_falsey
      end
    end

    context 'when possible choices is empty' do
      let(:decks) { 4 }
      let(:possible_choices) { [] }
      let(:builder) { ShipBuilder.new(decks, possible_choices, input, strategy) }

      it 'should return false' do
        expect(builder.try_to_place!).to be_falsey
      end
    end

    #todo
    context 'when use undefined place strategy' do
      it 'should fail with message' do
        builder = ShipBuilder.new(4, @board, @possible_choices, @undefined_strategy)
        expect { builder.try_to_place! }.to raise_error('Undefined placing rules')
      end
    end

    context 'when use undefined input type' do
      it 'should fail with message' do
        builder = ShipBuilder.new(4, @board, @possible_choices, @undefined_input)
        allow(builder).to receive(:puts)
        expect { builder.try_to_place! }.to raise_error('Undefined input type')
      end
    end

    context 'when auto ship building, place strategy is default' do
      it 'should ship with size == 1 for decks size == 1' do
        deck_size = 1
        builder = ShipBuilder.new(deck_size, @board, @possible_choices, @default_game)
        ship = builder.try_to_place!
        expect(ship).to be_a_kind_of Ship
        expect(ship.coords.count).to be == deck_size
      end

      it 'should ship with size == 2 for decks size == 2' do
        deck_size = 2
        builder = ShipBuilder.new(deck_size, @board, @possible_choices, @default_game)
        ship = builder.try_to_place!
        expect(ship).to be_a_kind_of Ship
        expect(ship.coords.count).to be == deck_size
      end

      it 'should ship with size == 3 for decks size == 3' do
        deck_size = 3
        builder = ShipBuilder.new(deck_size, @board, @possible_choices, @default_game)
        ship = builder.try_to_place!
        expect(ship).to be_a_kind_of Ship
        expect(ship.coords.count).to be == deck_size
      end

      it 'should ship with size == 4 for decks size == 4' do
        deck_size = 4
        builder = ShipBuilder.new(deck_size, @board, @possible_choices, @default_game)
        ship = builder.try_to_place!
        expect(ship).to be_a_kind_of Ship
        expect(ship.coords.count).to be == deck_size
      end
    end

    context 'when auto ship building, place strategy is hell tetris' do
      it 'should ship with size == 1 for decks size == 1' do
        deck_size = 1
        builder = ShipBuilder.new(deck_size, @board, @possible_choices, @tetris_game)
        ship = builder.try_to_place!
        expect(ship).to be_a_kind_of Ship
        expect(ship.coords.count).to be == deck_size
      end

      it 'should ship with size == 2 for decks size == 2' do
        deck_size = 2
        builder = ShipBuilder.new(deck_size, @board, @possible_choices, @tetris_game)
        ship = builder.try_to_place!
        expect(ship).to be_a_kind_of Ship
        expect(ship.coords.count).to be == deck_size
      end

      it 'should ship with size == 3 for decks size == 3' do
        deck_size = 3
        builder = ShipBuilder.new(deck_size, @board, @possible_choices, @tetris_game)
        ship = builder.try_to_place!
        expect(ship).to be_a_kind_of Ship
        expect(ship.coords.count).to be == deck_size
      end

      it 'should ship with size == 4 for decks size == 4' do
        deck_size = 4
        builder = ShipBuilder.new(deck_size, @board, @possible_choices, @tetris_game)
        ship = builder.try_to_place!
        expect(ship).to be_a_kind_of Ship
        expect(ship.coords.count).to be == deck_size
      end
    end

    context 'when user enter wrong cell coordinates' do
      it 'should show message "Incorrect coordinates, please enter coordinates in range A1-J10"' do
        builder = ShipBuilder.new(4, @board, @possible_choices, @manual_input)
        allow(ConsoleInput).to receive(:gets).and_return('M1', 'A2', 'A3', 'A4', 'A5')
        allow(ConsoleOutput).to receive(:print)
        allow(ConsoleOutput).to receive(:puts)
        expect(ConsoleOutput).to receive(:puts).with('Incorrect coordinates, please enter coordinates in range A1-J10')
        builder.try_to_place!
      end
    end

    context 'when user enter cell coordinates that already used' do
      it 'should show message "Bad place for deck, please choose again"' do
        possible_choices = @possible_choices - [Coordinates.new(0, 0)]
        builder = ShipBuilder.new(4, @board, possible_choices, @manual_input)
        allow(ConsoleInput).to receive(:gets).and_return('A1', 'A2', 'A3', 'A4', 'A5')
        allow(ConsoleOutput).to receive(:print)
        allow(ConsoleOutput).to receive(:puts)
        expect(ConsoleOutput).to receive(:puts).with('Bad place for deck, please choose again')
        builder.try_to_place!
      end
    end

    context 'when too little place for ship' do
      it 'should show message "Bad place for ship, please choose again"' do
        ship1 = Ship.new([Coordinates.new(0, 2), Coordinates.new(1, 2),
                          Coordinates.new(2, 2), Coordinates.new(3, 2)])
        ship2 = Ship.new([Coordinates.new(3, 0)])

        possible_choices = @possible_choices - (ship1.filled_space + ship2.filled_space)

        builder = ShipBuilder.new(4, @board, possible_choices, @manual_input)
        allow(ConsoleInput).to receive(:gets).and_return('A1', 'J1', 'J2', 'J3', 'J4')
        allow(ConsoleOutput).to receive(:print)
        allow(ConsoleOutput).to receive(:puts)
        expect(ConsoleOutput).to receive(:puts).with('Bad place for ship, please choose again')
        builder.try_to_place!
      end
    end

    context 'when manual ship building' do
      it 'should return ship with size == 1 for decks size == 1' do
        deck_size = 1
        builder = ShipBuilder.new(deck_size, @board, @possible_choices, @manual_input)
        allow(ConsoleInput).to receive(:gets).and_return('A1')
        allow(ConsoleOutput).to receive(:print)
        allow(ConsoleOutput).to receive(:puts)
        ship = builder.try_to_place!
        expect(ship).to be_a_kind_of Ship
        expect(ship.coords.count).to be == deck_size
      end

      it 'should return ship with size == 2 for decks size == 2' do
        deck_size = 2
        builder = ShipBuilder.new(deck_size, @board, @possible_choices, @manual_input)
        allow(ConsoleInput).to receive(:gets).and_return('A1', 'A2')
        allow(ConsoleOutput).to receive(:print)
        allow(ConsoleOutput).to receive(:puts)
        ship = builder.try_to_place!
        expect(ship).to be_a_kind_of Ship
        expect(ship.coords.count).to be == deck_size
      end

      it 'should return ship with size == 3 for decks size == 3' do
        deck_size = 3
        builder = ShipBuilder.new(deck_size, @board, @possible_choices, @manual_input)
        allow(ConsoleInput).to receive(:gets).and_return('A1', 'A2', 'A3')
        allow(ConsoleOutput).to receive(:print)
        allow(ConsoleOutput).to receive(:puts)
        ship = builder.try_to_place!
        expect(ship).to be_a_kind_of Ship
        expect(ship.coords.count).to be == deck_size
      end

      it 'should return ship with size == 4 for decks size == 4' do
        deck_size = 4
        builder = ShipBuilder.new(deck_size, @board, @possible_choices, @manual_input)
        allow(ConsoleInput).to receive(:gets).and_return('A1', 'A2', 'A3', 'A4')
        allow(ConsoleOutput).to receive(:print)
        allow(ConsoleOutput).to receive(:puts)
        ship = builder.try_to_place!
        expect(ship).to be_a_kind_of Ship
        expect(ship.coords.count).to be == deck_size
      end
    end

    context 'when builder use undefined input type class' do
      it 'should show message "Undefined input type"' do
        builder = ShipBuilder.new(4, @board, @possible_choices, @undefined_input)
        allow(ConsoleOutput).to receive(:puts)
        expect { builder.try_to_place! }.to raise_error('Undefined input type')
      end
    end
  end
end
