class VirtualPlayer < SomePlayer
  def initialize(board)
    super(board)

    @possible_inputs = @board.field.select do |cell|
      cell.state == :unchecked
    end.map(&:position)

    @recommended_inputs = @possible_inputs
    @hits_sequence = []
  end

  def shoot
    coord = get_coord
    return nil unless coord

    was_hit = @shooter.shoot(coord)

    if was_hit
      @hits_sequence << coord

      strategy = @board.strategy.new(@hits_sequence, dimension_for_shoot)
      @recommended_inputs = strategy.places.select do |place|
        @possible_inputs.include?(place)
      end
    end

    # when destroy ship
    if @recommended_inputs.empty?
      @hits_sequence.each { |hit_coord| @possible_inputs -= Ship.cell_surrounding(hit_coord) }
      @hits_sequence.clear
      @recommended_inputs = @possible_inputs
    end

    Shot.new(was_hit, coord)
  end

  def serialize(file_name)
    CSV.open(file_name, 'wb') do |csv|
      csv << @possible_inputs.map(&:to_csv)
      csv << @recommended_inputs.map(&:to_csv)
      csv << @hits_sequence.map(&:to_csv)
    end
  end

  def deserialize(file_name)
    csv = CSV.open(file_name, 'rb')
    csv.convert do |coord_str|
      coord_arr = coord_str.parse_csv
      x = coord_arr[0].to_i
      y = coord_arr[1].to_i

      Coordinates.new(x, y)
    end

    @possible_inputs = csv.readline
    @recommended_inputs = csv.readline
    @hits_sequence = csv.readline
  end

  private

  def get_coord
    input = RandomInput.new(@recommended_inputs).fetch
    @recommended_inputs -= [input]
    @possible_inputs -= [input]
    input
  end

  def dimension_for_shoot
    dimension = @board.strategy.max_dimension
    biggest_live_ship_size = 0

    @board.ships.values.flatten(1).each do |ship|
      next if ship.killed?
      biggest_live_ship_size = ship.size if biggest_live_ship_size < ship.size
    end

    dimension = biggest_live_ship_size if biggest_live_ship_size < dimension
    dimension
  end
end

class Coordinates
  def to_csv(options = {})
    pair = [x, y]
    CSV.generate_line(pair, options).chomp
  end
end
