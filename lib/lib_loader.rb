# directories
%w(
  /
  cells
  exceptions
  inputs
  players
  strategies
  trees
).each { |dir| $LOAD_PATH.unshift(File.join(File.dirname(__FILE__), dir)) }

# requires
%w(
  board_template_exception
  csv_file_content_exception
  ship_placing_exception
  coordinates
  cell
  deck
  some_input
  random_input
  queue_input
  base_strategy
  default_strategy
  hell_tetris_strategy
  tree
  coordinates_tree
  input_variations_tree
  input_variations_tree_builder
  ship
  shipbuilder
  board
  shot
  shooter
  some_player
  real_player
  virtual_player
  game
  csv
  json
).each { |path| require path }
