require 'inputs/some_input'
require 'inputs/queue_input'

describe QueueInput do
  describe '#fetch' do
    let(:val1) { [1, 1] }
    let(:val2) { [2, 2] }
    let(:val3) { [3, 3] }
    let(:inputs) { [val1, val2, val3] }
    let(:input) { QueueInput.new(inputs) }

    context 'when fetch calls' do
      it 'should return each value in FIFO order' do
        expect(input.fetch).to eq val1
        expect(input.fetch).to eq val2
        expect(input.fetch).to eq val3
      end

      it 'should return nil, if all values have been returned' do
        input.fetch
        input.fetch
        input.fetch

        expect(input.fetch).to be_nil
      end
    end
  end
end
