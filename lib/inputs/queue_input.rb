class QueueInput < SomeInput
  def fetch
    @inputs.delete_at(0)
  end
end
