class Board
  ROWS = 10
  COLS = 10

  attr_reader :field
  attr_reader :ships

  def self.clean_outer(coords)
    clean_coords = Array.new(coords)
    clean_coords.delete_if do |coord|
      (coord.x < 0) || (coord.x > 9) || (coord.y < 0) || (coord.y > 9)
    end
  end

  def initialize(template, placing_strategy)
    template.each do |key, value|
      is_correct_key = key.is_a?(Fixnum) && key > 0
      is_correct_value = value.is_a?(Fixnum) && value > 0

      unless is_correct_key && is_correct_value
        fail BoardTemplateException.new('Incorrect ship\'s template given')
      end
    end

    @template = template
    @placing_strategy = placing_strategy

    initialize_ships
    initialize_field
    initialize_possible_choices
  end

  def strategy
    @placing_strategy
  end

  def all_ships_placed?
    @ships.each do |ship_size, ships|
      return false if ships.count < @template[ship_size]
    end

    true
  end

  def any_life?
    @ships.values.flatten(1).any? { |ship| !ship.killed? }
  end

  def place_ship(ship)
    ship_size = ship.size
    return nil unless @template.key? ship_size
    return nil unless @ships[ship_size].count < @template[ship_size]

    input = QueueInput.new(ship.coords)
    builder = ShipBuilder.new(ship_size, @possible_choices, input, @placing_strategy)

    fail ShipPlacingException.new('Ship wasn\'t built') unless builder.try_to_place!

    ship.decks.each do |deck|
      i = @field.index { |cell| cell.position == deck.position }
      @field[i] = deck
    end

    @ships[ship_size] << ship
    @possible_choices -= ship.filled_space

    ship
  end

  def fill_with_ships!
    initialize_field
    initialize_ships
    initialize_possible_choices

    @template.each_pair do |deck_size, count|
      count.times do
        input = RandomInput.new(@possible_choices)
        builder = ShipBuilder.new(deck_size, @possible_choices, input, @placing_strategy)
        ship = builder.try_to_place!

        fail ShipPlacingException.new('Ship wasn\'t built') unless ship

        ship.decks.each do |deck|
          i = @field.index { |cell| cell.position == deck.position }
          @field[i] = deck
        end

        @ships[deck_size] << ship
        @possible_choices -= ship.filled_space
      end
    end
  end

  def serialize(file_name)
    CSV.open(file_name, 'wb') do |csv|
      (0..ROWS - 1).each do |j|
        row = []

        (0..COLS - 1).each do |i|
          cell = @field[j * COLS + i]
          row << cell.to_csv
        end

        csv << row
      end
    end
  end

  def deserialize(file_name)
    csv = CSV.open(file_name, 'rb')
    csv.convert do |cell_str|
      cell_arr = cell_str.parse_csv
      state = cell_arr[2].to_i

      case state
      when Cell::STATE[:unchecked], Cell::STATE[:checked] then cell_arr.to_cell
      when Deck::STATE[:unchecked], Deck::STATE[:hit] then cell_arr.to_deck
      else fail CSVFileContentException.new('CSV contains undefined cell\'s state')
      end
    end

    field = csv.read.flatten(1)
    ships = ships_from_field(field)

    unless check_ships_with_template(ships)
      fail CSVFileContentException.new('CSV incorrect ships configuration for boards template')
    end
    fail CSVFileContentException.new('CSV ships crossing detected') if ships_has_crossing(ships)

    @field = field
    initialize_ships(ships)
    initialize_possible_choices(ships)
  end

  private

  def initialize_field
    @field = Array.new(COLS) do |x|
      Array.new(ROWS) do |y|
        Cell.new(Coordinates.new(x, y))
      end
    end.flatten(1)
  end

  def initialize_ships(ships = nil)
    @ships = {}
    @template.each_key { |key| @ships[key] = [] }
    ships.values.flatten(1).each { |ship| @ships[ship.size] << ship } if ships
  end

  def initialize_possible_choices(ships = nil)
    @possible_choices = Array.new(COLS) do |x|
      Array.new(ROWS) do |y|
        Coordinates.new(x, y)
      end
    end.flatten(1)

    ships.values.flatten(1).each { |ship| @possible_choices -= ship.filled_space } if ships
  end

  def ships_from_field(field)
    fld = Array.new(field)
    fld.delete_if { |cell| cell.class == Cell }
    ships = {}

    while fld.any?
      ship = pop_ship(fld)
      ships[ship.size] = [] unless ships.key? ship.size
      ships[ship.size] << ship
    end

    ships
  end

  def pop_ship(field)
    return nil unless field.first.class == Deck
    deck = field.first

    decks = [deck] + related_decks(field, [deck])
    decks.each { |dck| field.delete(dck) }
    Ship.new(decks)
  end

  def related_decks(field, decks)
    coords = field.map(&:position)
    positions = decks.map(&:position)

    strategy = @placing_strategy.new(positions, @placing_strategy.max_dimension)
    places = strategy.places
    positions = coords & places

    related = field.select { |cell| positions.include? cell.position }
    related += related_decks(field, decks + related) if related.any?
    related
  end

  def check_ships_with_template(ships)
    ships.each do |key, value|
      return false unless @template.key?(key) && value.count <= @template[key]
    end

    true
  end

  def ships_has_crossing(ships)
    ships_arr = ships.values.flatten(1)
    total_space = []

    ships_arr.each do |ship|
      crossing_space = total_space & ship.coords

      return true if crossing_space.any?

      total_space |= ship.filled_space
    end

    false
  end
end

class Cell
  STATE = {
    unchecked: 0,
    checked: 1
  }.freeze

  def to_csv(options = {})
    cell_state = case state
                 when :unchecked then STATE[:unchecked]
                 when :checked then STATE[:checked]
                 else fail 'Undefined cell\'s state'
                 end

    cell_arr = [position.x, position.y, cell_state]
    CSV.generate_line(cell_arr, options).chomp
  end
end

class Deck
  STATE = {
    unchecked: 2,
    hit: 3
  }.freeze

  def to_csv(options = {})
    deck_state = case state
                 when :unchecked then STATE[:unchecked]
                 when :hit then STATE[:hit]
                 else fail 'Undefined deck\'s state'
                 end

    deck_arr = [position.x, position.y, deck_state]
    CSV.generate_line(deck_arr, options).chomp
  end
end

class Array
  def to_cell
    x = self[0].to_i
    y = self[1].to_i
    state = self[2].to_i

    cell = Cell.new(Coordinates.new(x, y))
    cell.check_it! if state == Cell::STATE[:checked]
    cell
  end

  def to_deck
    x = self[0].to_i
    y = self[1].to_i
    state = self[2].to_i

    deck = Deck.new(Coordinates.new(x, y))
    deck.check_it! if state == Deck::STATE[:hit]
    deck
  end
end
