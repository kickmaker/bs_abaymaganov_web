require 'coordinates'
require 'board'
require 'strategies/base_strategy'
require 'strategies/default_strategy'
require 'strategies/hell_tetris_strategy'

describe HellTetrisStrategy do
  describe '#places' do
    context 'when positions include turn places for sheep' do
      it 'should return empty places' do
        positions = [Coordinates.new(1, 1), Coordinates.new(1, 2), Coordinates.new(1, 4)]
        decks = 4
        strategy = HellTetrisStrategy.new(positions, decks)
        places = strategy.places
        expect(places).to be_empty
      end
    end

    context 'when positions count more or equal than decks count' do
      it 'should return empty places' do
        positions = [Coordinates.new(1, 1), Coordinates.new(1, 2),
                     Coordinates.new(1, 3), Coordinates.new(1, 4)]
        decks = positions.count
        strategy = HellTetrisStrategy.new(positions, decks)
        places = strategy.places
        expect(places).to be_empty

        decks = positions.count - 1
        strategy = HellTetrisStrategy.new(positions, decks)
        places = strategy.places
        expect(places).to be_empty
      end
    end

    context 'when positions is empty and decks count greater than zero' do
      it 'should return places that includes all coordinates of the field' do
        field_places = Array.new(10) do |x|
          Array.new(10) do |y|
            Coordinates.new(x, y)
          end
        end.flatten(1)

        strategy = HellTetrisStrategy.new([], 4)
        places = strategy.places
        expect(places).to be == field_places
      end
    end

    context 'when positions count equal 2, decks count equal 4, vertical ship in center' do
      it 'should return 6 places: top, bottom and places from left and right sides of the ship' do
        positions = [Coordinates.new(3, 3), Coordinates.new(3, 4)]
        expected_places = [Coordinates.new(3, 2)]
        expected_places << Coordinates.new(3, 5)
        expected_places << Coordinates.new(2, 3)
        expected_places << Coordinates.new(2, 4)
        expected_places << Coordinates.new(4, 3)
        expected_places << Coordinates.new(4, 4)

        strategy = HellTetrisStrategy.new(positions, 4)
        places = strategy.places
        expect(places.count).to be == 6
        expected_places.each do |place|
          expect(places).to include place
        end
      end
    end

    context 'when positions count equal 3, decks count equal 4, vertical ship in center' do
      it 'should return 8 places: top, bottom and places from left and right sides of the ship' do
        positions = [Coordinates.new(3, 3), Coordinates.new(3, 4), Coordinates.new(3, 5)]
        expected_places = [Coordinates.new(3, 2)]
        expected_places << Coordinates.new(3, 6)
        expected_places << Coordinates.new(2, 3)
        expected_places << Coordinates.new(2, 4)
        expected_places << Coordinates.new(2, 5)
        expected_places << Coordinates.new(4, 3)
        expected_places << Coordinates.new(4, 4)
        expected_places << Coordinates.new(4, 5)

        strategy = HellTetrisStrategy.new(positions, 4)
        places = strategy.places
        expect(places.count).to be == 8
        expected_places.each do |place|
          expect(places).to include place
        end
      end
    end

    context 'when positions count equal 2, decks count equal 4, horizontal ship in center' do
      it 'should return 6 places: top, bottom and places from left and right sides of the ship' do
        positions = [Coordinates.new(3, 3), Coordinates.new(4, 3)]
        expected_places = [Coordinates.new(2, 3)]
        expected_places << Coordinates.new(5, 3)
        expected_places << Coordinates.new(3, 2)
        expected_places << Coordinates.new(4, 2)
        expected_places << Coordinates.new(3, 4)
        expected_places << Coordinates.new(4, 4)

        strategy = HellTetrisStrategy.new(positions, 4)
        places = strategy.places
        expect(places.count).to be == 6
        expected_places.each do |place|
          expect(places).to include place
        end
      end
    end

    context 'when positions count equal 3, decks count equal 4, horizontal ship in center' do
      it 'should return 8 places: top, bottom and places from left and right sides of the ship' do
        positions = [Coordinates.new(3, 3), Coordinates.new(4, 3), Coordinates.new(5, 3)]
        expected_places = [Coordinates.new(2, 3)]
        expected_places << Coordinates.new(6, 3)
        expected_places << Coordinates.new(3, 2)
        expected_places << Coordinates.new(4, 2)
        expected_places << Coordinates.new(5, 2)
        expected_places << Coordinates.new(3, 4)
        expected_places << Coordinates.new(4, 4)
        expected_places << Coordinates.new(5, 4)

        strategy = HellTetrisStrategy.new(positions, 4)
        places = strategy.places
        expect(places.count).to be == 8
        expected_places.each do |place|
          expect(places).to include place
        end
      end
    end

    context 'when positions count equal 3, decks count equal 4, tetris ship in center' do
      it 'should return 4 places' do
        positions = [Coordinates.new(3, 3), Coordinates.new(3, 4), Coordinates.new(2, 4)]
        expected_places = [Coordinates.new(3, 2)]
        expected_places << Coordinates.new(3, 5)
        expected_places << Coordinates.new(1, 4)
        expected_places << Coordinates.new(4, 4)

        strategy = HellTetrisStrategy.new(positions, 4)
        places = strategy.places
        expect(places.count).to be == 4
        expected_places.each do |place|
          expect(places).to include place
        end
      end
    end
  end
end
