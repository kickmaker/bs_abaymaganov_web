class BaseStrategy
  def self.max_dimension
    10
  end

  def initialize(positions, decks)
    @positions = positions.sort { |c1, c2| c1.y <=> c2.y }.sort { |c1, c2| c1.x <=> c2.x }
    @decks = decks
  end
end
